package domain

type Player struct {
	game *Game
}

func (p Player) IsInGame() bool {
	return p.game != nil
}

func (p *Player) Join(game *Game) {
	p.game = game
}
