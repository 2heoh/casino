package domain

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestPlayer(t *testing.T) {
	t.Run("is not in game by default", func(t *testing.T) {
		player := newPlayer()

		playerIsInGame := player.IsInGame()

		assert.False(t, playerIsInGame)
	})

	t.Run("can join game", func(t *testing.T) {
		player := newPlayer()

		player.Join(newGame())

		assert.True(t, player.IsInGame())
	})
}

func newGame() *Game {
	return &Game{}
}

func newPlayer() Player {
	return Player{}
}
